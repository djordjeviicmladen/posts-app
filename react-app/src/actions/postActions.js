import { FETCH_POSTS, NEW_POST, FETCH_POST } from './types';
import { setLoader } from './uiActions';

export const fetchPosts = () => dispatch => { 
  dispatch(setLoader(true));
  fetch('http://127.0.0.1:8000/api/posts')
    .then(res => res.json())
    .then(posts => {
      setTimeout(() => {
        dispatch(
          setPosts(posts)
        )
        dispatch(setLoader(false))
      }, 2500)
    }
  );
};

export const createPost = postData => dispatch => {
  fetch('http://127.0.0.1:8000/api/posts', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(postData)
  })
    .then(res => res.json())
    .then(newItem => 
      dispatch(
        newPost(newItem)
      )
    );
};

export const fetchPost = postId => dispatch => { 
  fetch(`http://127.0.0.1:8000/api/posts/${postId}`)
    .then(res => res.json())
    .then(postItem =>{
      dispatch(
        setPost(postItem)
      )
    }
  );
};

const setPosts = posts => {
  return {
    type: FETCH_POSTS,
    payload: posts
  }
};

const newPost = newItem => {
  return {
    type: NEW_POST,
    payload: newItem
  }
};

const setPost = postItem => {
  return {
    type: FETCH_POST,
    payload: postItem
  }
};