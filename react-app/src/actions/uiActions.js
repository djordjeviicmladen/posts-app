import { SET_LOADER } from './types';

export const setLoader = isLoad => ({
  type: SET_LOADER,
  payload: isLoad
});