export const FETCH_POSTS = 'FETCH_POSTS';
export const NEW_POST = 'NEW_POST';
export const FETCH_POST = 'FETCH_POST';
export const SET_LOADER = 'SET_LOADER';