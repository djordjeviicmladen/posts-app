import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchPost } from '../actions/postActions';

import { Container, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';

class Post extends Component {
  componentWillMount() {
    this.props.fetchPost(this.props.location.pathname.split('/post/')[1]);
  }

  render() {
    return (
      <Container>
        <br />
        <ListGroup>
          <ListGroupItem>
            <ListGroupItemHeading>
              {this.props.post.title}
            </ListGroupItemHeading>
            <ListGroupItemText>
              {this.props.post.body}
            </ListGroupItemText>
          </ListGroupItem>
        </ListGroup>
      </Container>
    );
  }
}

Post.propTypes = {
  fetchPost: PropTypes.func.isRequired,
  postItem: PropTypes.object
};

const mapStateToProps = state => ({
  post: state.posts.postItem
});

export default connect(mapStateToProps, { fetchPost })(Post);