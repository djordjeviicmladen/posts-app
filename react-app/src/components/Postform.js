import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { createPost } from '../actions/postActions';

import { Container, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';

class PostForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      body: '',
      message: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.message = this.message.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const newItem = {
      title: this.state.title,
      body: this.state.body
    };
    
    this.props.createPost(newItem);

    this.setState({
      title: '',
      body: ''
    });
  }

  message() {
    if(this.state.title === '' || this.state.body === '') {
      this.setState({message: {class: 'danger', text: 'Not submited'}})
    } else {
      this.setState({message: {class: 'success', text: 'Submited'}})
    }
  }

  showMessage() {
    if (this.state.message !== '' ) {   
      return (<Alert color={this.state.message.class}>{this.state.message.text}</Alert>);
    }
  }

  render() {
    return (
      <Container>
        <h2 className="text-center">Add post</h2>        
        <br />
        {this.showMessage()}
        <hr />
        <Form onSubmit={this.onSubmit}>
          <FormGroup>
            <Label>Title</Label>
            <Input type="text" name="title" onChange={this.onChange} value={this.state.title} />
          </FormGroup>

          <FormGroup>
            <Label>Body</Label>
            <Input type="textarea" name="body" onChange={this.onChange} value={this.state.body} rows="5" />
          </FormGroup>
          <Button type="submit" outline color="info" onClick={() => this.message()}>Submit</Button>
        </Form>
      </Container>
    );
  }
}

PostForm.propTypes = {
  createPost: PropTypes.func.isRequired
};

export default connect(null, { createPost })(PostForm);