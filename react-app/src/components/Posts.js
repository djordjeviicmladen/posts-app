import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { fetchPosts, fetchPost } from '../actions/postActions';

import { Container, Card, CardText, CardBody, CardTitle, Button, Row, Col, Form, Input } from 'reactstrap';
import Loader from './loader/Loader';
import '../App.css';

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    this.props.fetchPosts();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newPost) {
      this.props.posts.unshift(nextProps.newPost);
    }
  }

  sendPost(id) {
    this.props.fetchPost(id);
  }

  postItems() {
    return this.props.posts.map(post => (
      <Col lg={4} key={post.id}>
        <Card>
          <CardBody>
            <CardTitle>{post.title}</CardTitle>
            <CardText>{post.body}</CardText>
            <Link to={`/post/${post.id}`} 
                className="text-white nounderline" 
                onClick={() => this.sendPost(post.id)}>
                  <Button color="primary">Read more ...</Button>
            </Link>
          </CardBody>
        </Card>
        <br />
      </Col>
    ));
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  searchPart() {
    return (
      <div>
        <br />
        <Row>
          <Col lg={{ size: 'auto', offset: 1 }}>
            <h1 className="text-center">Posts</h1>
          </Col>
          <Col lg={{ size: 'auto', offset: 6 }}>
            <Form inline>
              <Input type="text" name="search" onChange={this.onChange} value={this.state.search} placeholder="Enter the keyword" />
            </Form>
          </Col>
        </Row>
        <hr />
      </div>
    );
  }

  searching() {
    if(this.state.search !== '') {
      return this.props.posts.filter(post => post.body.includes(this.state.search)).map(post => (
        <Col lg={4} key={post.id}>
          <Card>
            <CardBody>
              <CardTitle>{post.title}</CardTitle>
              <CardText>{post.body}</CardText>
              <Link to={`/post/${post.id}`} 
                  className="text-white nounderline" 
                  onClick={() => this.sendPost(post.id)}>
                    <Button color="primary">Read more ...</Button>
              </Link>
            </CardBody>
          </Card>
          <br />
        </Col>
      ));
    } else {
      return this.postItems();
    }
    
  }

  render() {
    return (
      <Container>
        {this.searchPart()}
        <Loader />
        <Row>{this.searching()}</Row>
      </Container>
    );
  }
}

Posts.propTypes = {
  fetchPosts: PropTypes.func.isRequired,
  fetchPost: PropTypes.func.isRequired,
  posts: PropTypes.array.isRequired,
  newPost: PropTypes.object
};

const mapStateToProps = state => ({
  posts: state.posts.items,
  newPost: state.posts.newItem
});

export default connect(mapStateToProps, { fetchPosts, fetchPost })(Posts);