import React, { Component } from 'react';
import { NavLink, Route, Switch } from 'react-router-dom';

import Posts from './Posts';
import Post from './Post';
import PostForm from './Postform';

import { Container, Navbar, Nav, NavItem } from 'reactstrap';
import '../App.css';

class Layout extends Component {
  render() {
    return (
        <div>
          <Container>
          <Navbar className="colorNavbar" light expand="md">
            <Nav>
              <NavItem>
                <NavLink className="nav-link nounderline" to="/">
                  Home
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link nounderline" to="/new-post">
                  New Post
                </NavLink>
              </NavItem>
            </Nav>
          </Navbar>
          </Container>
          
          <Switch>
            <Route exact path="/" component={Posts}/>
            <Route path="/new-post" component={PostForm}/>
            <Route path="/post/:id" component={Post} />
          </Switch>
        </div>
    );
  }
}

export default Layout;