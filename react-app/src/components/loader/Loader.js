import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setLoader } from '../../actions/uiActions';

import './Loader.css';

class Loader extends Component {
  isLoading() {
    if (this.props.ui.isLoading === true){
      return (
        <div className="lds-spinner">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      );
    } else {
      return;
    }
  }

  render() {
    return (
      <div>
        {this.isLoading()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ui: state.ui
});

export default connect(mapStateToProps, { setLoader })(Loader);