import { combineReducers } from 'redux';
import postReducer from './postReducer';
import uiReducer from './uiReducer';

export default combineReducers({
  posts: postReducer,
  ui: uiReducer
});