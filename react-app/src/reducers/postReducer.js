import { FETCH_POSTS, NEW_POST, FETCH_POST } from '../actions/types';

const initialState = {
  items: [],
  newItem: {},
  postItem: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        items: action.payload
      };
    case NEW_POST:
      return {
        ...state,
        newItem: action.payload
      };
    case FETCH_POST:
      return {
        ...state,
        postItem: action.payload
      };
    default:
      return state;
  }
}