<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\PostsController@index')->name('posts.index');
Route::get('/create', 'Admin\PostsController@create')->name('posts.create');
Route::post('/', 'Admin\PostsController@store')->name('posts.store');
Route::get('/post/{post}', 'Admin\PostsController@post')->name('posts.post');