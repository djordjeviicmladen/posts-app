@extends('layout.master')

@section('main-content')
    <br />
    <div>
        <div class="card card-outline-primary">
            <div class="card-header">
                <h4 class="m-b-0 text-black">Add Post</h4>
            </div>
            <div class="card-body">
                <form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
                    <div class="form-body">
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input
                                            type="text"
                                            name="title"
                                            class="form-control"
                                            placeholder="Enter title "
                                            required
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Body</label>
                                    <input
                                            type="textarea"
                                            name="body"
                                            class="form-control"
                                            placeholder="Enter body text"
                                            required
                                    >
                                </div>
                            </div>
                        </div>
                        @csrf
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save
                        </button>
                        <a class="btn btn-inverse" href="{{route('posts.index')}}">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection