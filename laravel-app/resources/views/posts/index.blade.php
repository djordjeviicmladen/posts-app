@extends('layout.master')

@section('main-content')
        <div class="row">
            <div class="col-md-4">
                <h2 class="text-center">Posts</h2>
            </div>
            <div class="col-md-4 ml-auto">
                <form action="" method="GET" class="form-inline">
                    <input type="text" name="search" class="form-control" placeholder="Enter a keyword">
                    <button type="submit"  class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <hr />
        <div class="row">
            @foreach($posts as $post)
            <div class="col-lg-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <p class="card-text">{{$post->body}}</p>
                        <a href="{{ route('posts.post', $post->id) }}" class="btn btn-primary">Read more ...</a>
                    </div>
                </div>
                <br />
            </div>
            @endforeach
        </div>
@endsection