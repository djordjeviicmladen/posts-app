@extends('layout.master')

@section('main-content')
        <br />
        <div class="row">
            <div class="col-lg-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title" style="color: red">{{$post->title}}</h5>
                        <p class="card-text">{{$post->body}}</p>
                    </div>
                </div>
                <br />
            </div>
        </div>
@endsection