<div class="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a class="nav-item nav-link" href="{{ route('posts.index') }}">Posts</a>
                <a class="nav-item nav-link" href="{{ route('posts.create') }}">New Post</a>
            </div>
        </div>
    </nav>
</div>
<br />