<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout.head')
</head>

<body class="fix-header fix-sidebar">
    <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="container">
                @include('layout.header')
                <div class="row">
                    <div class="col-12">
                        @yield('main-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layout.scripts')
</body>
</html>