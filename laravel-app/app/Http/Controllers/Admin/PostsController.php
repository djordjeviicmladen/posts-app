<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Api\PostsRequest;
use App\Http\Controllers\Controller;
use App\Models\Posts;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->input('search');
        
        if($request === ''){
            $posts = Posts::all();
        } else {
            $posts = Posts::where('title', 'like', '%' . $search . '%')
                            ->orWhere('body', 'like', '%' . $search . '%')
                            ->get();
        }
        
        return view('posts.index', ['posts' => $posts]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(PostsRequest $request)
    {
        try {
            Posts::create($request->all());

            return redirect()->route('posts.index')->with('success', 'Post successfully saved.');
        }
        catch(ValidationException $e){
            return redirect()->back()->with('error', 'Post is not saved.');
        }
    }

    public function post(Posts $post)
    { 
        return view('posts.post', ['post' => $post]);
    }
}