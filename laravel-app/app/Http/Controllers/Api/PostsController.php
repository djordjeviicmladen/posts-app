<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\PostsRequest;
use App\Http\Controllers\Controller;
use App\Models\Posts;

class PostsController extends Controller
{
    public function getAll()
    {
        return Posts::all();
    }

    public function add(PostsRequest $request)
    {
        return Posts::create($request->all());
    }

    public function get($id)
    {
        return Posts::find($id);
    }

    public function edit($id, PostsRequest $request)
    {
        return Posts::get($id)->fill($request->all())->save();
    }

    public function delete($id)
    {
        return Posts::create($id)->delete();
    }
}
